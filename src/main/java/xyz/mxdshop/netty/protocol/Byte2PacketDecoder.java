package xyz.mxdshop.netty.protocol;

import io.netty.buffer.ByteBuf;
import xyz.mxdshop.netty.domain.BasePacket;
import xyz.mxdshop.netty.serialize.Serializer;

/**
 * 消息解码类
 * @author apple
 */
public class Byte2PacketDecoder extends BasePacketCodeC {

    private static final Byte2PacketDecoder INSTANCE = new Byte2PacketDecoder();

    private Byte2PacketDecoder() {}


    public static Byte2PacketDecoder getInstance() {
        return INSTANCE;
    }

    public BasePacket decode(ByteBuf byteBuf) {
        // 跳过魔数
        byteBuf.skipBytes(4);
        // 指令
        byte command = byteBuf.readByte();
        // 序列化类型
        byte serializeType = byteBuf.readByte();
        int length = byteBuf.readInt();
        byte[] dataBytes = new byte[length];
        byteBuf.readBytes(dataBytes);

        Class<? extends BasePacket> messageType = getMessageType(command);
        if (null == messageType) {
            return null;
        }
        Serializer serializer = getSerializer(serializeType);
        if (null == serializer) {
            return null;
        }

        return serializer.deserialize(dataBytes, messageType);
    }
}
