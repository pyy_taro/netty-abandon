package xyz.mxdshop.netty.protocol;

import io.netty.buffer.ByteBuf;
import xyz.mxdshop.netty.domain.BasePacket;
import xyz.mxdshop.netty.serialize.Serializer;

/**
 * 消息编码类
 * @author pyy
 */
public class Packet2ByteEncoder extends BasePacketCodeC {

    private static final Packet2ByteEncoder INSTANCE = new Packet2ByteEncoder();

    private Packet2ByteEncoder() {}

    public static Packet2ByteEncoder getInstance() {
        return INSTANCE;
    }

    /**
     * 消息编码
     * @param packet
     *      消息体
     */
    public void encode(ByteBuf out, BasePacket packet) {

        byte[] dataBytes = Serializer.DEFAULT.serialize(packet);
        out.writeInt(MAGIC_NUMBER);
        out.writeByte(packet.getCommand());
        out.writeByte(Serializer.DEFAULT.currentSerializeType());
        out.writeInt(dataBytes.length);
        out.writeBytes(dataBytes);
    }
}
