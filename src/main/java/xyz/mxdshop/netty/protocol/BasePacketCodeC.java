package xyz.mxdshop.netty.protocol;

import xyz.mxdshop.netty.domain.*;
import xyz.mxdshop.netty.serialize.Serializer;

import java.util.HashMap;
import java.util.Map;

/**
 * 编解码父类
 * @author apple
 */
public abstract class BasePacketCodeC {

    static final int MAGIC_NUMBER = 0xc0de2020;
    /**
     * 操作对应的消息类型
     */
    private static final Map<Byte, Class<? extends BasePacket>> PACKET_TYPE_MAP;
    private static final Map<Byte, Serializer> SERIALIZER_MAP;

    static {
        PACKET_TYPE_MAP = new HashMap<>(16);
        PACKET_TYPE_MAP.put(CommandConstant.LOGIN_REQ, LoginRequestMessage.class);
        PACKET_TYPE_MAP.put(CommandConstant.LOGIN_RSP, LoginResponseMessage.class);
        PACKET_TYPE_MAP.put(CommandConstant.HEARTBEAT_REQ, HeartBeatRequestMessage.class);
        PACKET_TYPE_MAP.put(CommandConstant.HEARTBEAT_RSP, HeartBeatResponseMessage.class);
        PACKET_TYPE_MAP.put(CommandConstant.ONLINE, OnlineMessage.class);
        PACKET_TYPE_MAP.put(CommandConstant.OFFLINE, OfflineMessage.class);
        PACKET_TYPE_MAP.put(CommandConstant.ONLINE_LIST_REQ, OnlineListRequestMessage.class);
        PACKET_TYPE_MAP.put(CommandConstant.ONLINE_LIST_RSP, OnlineListResponseMessage.class);
        PACKET_TYPE_MAP.put(CommandConstant.SHAKE_REQ, ShakeRequestMessage.class);
        PACKET_TYPE_MAP.put(CommandConstant.SHAKE_RSP, ShakeResponseMessage.class);
        PACKET_TYPE_MAP.put(CommandConstant.TEXT_REQ, TextRequestMessage.class);
        PACKET_TYPE_MAP.put(CommandConstant.TEXT_RSP, TextResponseMessage.class);

        SERIALIZER_MAP = new HashMap<>(16);
        SERIALIZER_MAP.put(Serializer.DEFAULT.currentSerializeType(), Serializer.DEFAULT);
    }

    protected Serializer getSerializer(byte serializeType) {

        return SERIALIZER_MAP.get(serializeType);
    }

    protected Class<? extends BasePacket> getMessageType(byte command) {

        return PACKET_TYPE_MAP.get(command);
    }
}
