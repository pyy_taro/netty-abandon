package xyz.mxdshop.netty.transform;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import xyz.mxdshop.netty.domain.BasePacket;
import xyz.mxdshop.netty.protocol.Packet2ByteEncoder;

/**
 * @author pyy
 * @date 2020-08-08 19:33
 **/
public class PacketEncoder extends MessageToByteEncoder<BasePacket> {

    @Override
    protected void encode(ChannelHandlerContext ctx, BasePacket msg, ByteBuf out) throws Exception {
        Packet2ByteEncoder.getInstance().encode(out, msg);
    }
}
