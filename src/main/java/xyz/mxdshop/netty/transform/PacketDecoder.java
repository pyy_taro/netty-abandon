package xyz.mxdshop.netty.transform;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import lombok.extern.slf4j.Slf4j;
import xyz.mxdshop.netty.protocol.Byte2PacketDecoder;

import java.util.List;

/**
 * @author pyy
 * @date 2020-08-08 19:12
 **/
@Slf4j
public class PacketDecoder extends ByteToMessageDecoder {

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) {
        out.add(Byte2PacketDecoder.getInstance().decode(in));
    }
}

