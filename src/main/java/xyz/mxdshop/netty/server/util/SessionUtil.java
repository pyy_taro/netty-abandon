package xyz.mxdshop.netty.server.util;

import io.netty.channel.Channel;
import xyz.mxdshop.netty.server.domain.NettyServerSession;

import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;

public class SessionUtil {

    private static final ConcurrentHashMap<NettyServerSession, Channel> CHANNEL_MAP = new ConcurrentHashMap<>();

    private static final ConcurrentHashMap<Long, NettyServerSession> SESSION_MAP = new ConcurrentHashMap<>();

    private static final ConcurrentHashMap<Channel, Long> CHANNEL_USER_ID_MAP = new ConcurrentHashMap<>();

    public static Channel getChannel(NettyServerSession session) {
        return CHANNEL_MAP.get(session);
    }

    public static Collection<Channel> getAllChannels() {
        return CHANNEL_MAP.values();
    }

    public static void addSession(Long userId, NettyServerSession session, Channel channel) {
        SESSION_MAP.putIfAbsent(userId, session);
        CHANNEL_MAP.putIfAbsent(session, channel);
        CHANNEL_USER_ID_MAP.putIfAbsent(channel, userId);
    }

    public static NettyServerSession getSession(Long userId) {
        return SESSION_MAP.get(userId);
    }

    public static Long getUserId(Channel channel) {
        return CHANNEL_USER_ID_MAP.get(channel);
    }

    public static void removeChannel(Channel channel) {
        Long userId = CHANNEL_USER_ID_MAP.remove(channel);
        if (userId != null) {
            NettyServerSession session = SESSION_MAP.remove(userId);
            CHANNEL_MAP.remove(session);
        }
    }

    public static Collection<NettyServerSession> getAllSessions() {
        return SESSION_MAP.values();
    }
}
