package xyz.mxdshop.netty.server;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import lombok.extern.slf4j.Slf4j;
import xyz.mxdshop.netty.server.handler.*;
import xyz.mxdshop.netty.transform.PacketDecoder;
import xyz.mxdshop.netty.transform.PacketEncoder;

import java.util.concurrent.TimeUnit;

/**
 * @author pyy
 */
@Slf4j
public class NettyServer {

    public static void main(String[] args) {

        ServerBootstrap serverBootstrap = new ServerBootstrap();
        NioEventLoopGroup bossGroup = new NioEventLoopGroup();
        NioEventLoopGroup workerGroup = new NioEventLoopGroup();
        try {
            serverBootstrap.group(bossGroup, workerGroup)
                    .option(ChannelOption.SO_BACKLOG, 1024)
                    .childOption(ChannelOption.TCP_NODELAY, true)
                    .childOption(ChannelOption.SO_KEEPALIVE, true)
                    .channel(NioServerSocketChannel.class)
                    .childHandler(new ChannelInitializer<NioSocketChannel>() {
                        @Override
                        protected void initChannel(NioSocketChannel ch) {
                            ch.pipeline().addLast(new LengthFieldBasedFrameDecoder(Integer.MAX_VALUE, 6, 4));
                            ch.pipeline().addLast("heartbeat", new NettyServerIdleStateHandler(10, 0, 0, TimeUnit.SECONDS));
                            ch.pipeline().addLast(new PacketDecoder());
                            ch.pipeline().addLast(new NettyServerHeartBeatHandler());
                            ch.pipeline().addLast(NettyServerLoginHandler.getInstance());
                            ch.pipeline().addLast("auth", new NettyServerAuthHandler());
                            ch.pipeline().addLast(NettyServerOnlineListHandler.getInstance());
                            ch.pipeline().addLast(NettyServerShakeMessageHandler.getInstance());
                            ch.pipeline().addLast(NettyServerTextMessageHandler.getInstance());
                            ch.pipeline().addLast(new PacketEncoder());
                        }
                    });
            ChannelFuture channelFuture = serverBootstrap.bind(8888).sync();
            channelFuture.channel().closeFuture().sync();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // 优雅关闭
            workerGroup.shutdownGracefully();
            bossGroup.shutdownGracefully();
        }

    }
}
