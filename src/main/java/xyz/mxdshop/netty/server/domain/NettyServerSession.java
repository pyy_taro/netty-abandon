package xyz.mxdshop.netty.server.domain;

import lombok.Data;

@Data
public class NettyServerSession {

    private long userId;

    private String username;
}
