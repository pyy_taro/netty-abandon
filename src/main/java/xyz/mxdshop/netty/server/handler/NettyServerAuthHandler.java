package xyz.mxdshop.netty.server.handler;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import lombok.extern.slf4j.Slf4j;
import xyz.mxdshop.netty.server.util.SessionUtil;

@Slf4j
public class NettyServerAuthHandler extends ChannelInboundHandlerAdapter {

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        if (SessionUtil.getUserId(ctx.channel()) != null) {
            ctx.pipeline().remove(this);
            super.channelRead(ctx, msg);
        } else {
            log.info("该客户端未鉴权，断开链接");
            ctx.channel().close();
        }
    }
}
