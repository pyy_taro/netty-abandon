package xyz.mxdshop.netty.server.handler;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import io.netty.handler.timeout.IdleStateHandler;
import lombok.extern.slf4j.Slf4j;
import xyz.mxdshop.netty.domain.OfflineMessage;
import xyz.mxdshop.netty.domain.OnlineMessage;
import xyz.mxdshop.netty.server.util.SessionUtil;

import java.util.concurrent.TimeUnit;

@Slf4j
public class NettyServerIdleStateHandler extends IdleStateHandler {
    public NettyServerIdleStateHandler(int readerIdleTimeSeconds, int writerIdleTimeSeconds, int allIdleTimeSeconds) {
        super(readerIdleTimeSeconds, writerIdleTimeSeconds, allIdleTimeSeconds);
    }

    public NettyServerIdleStateHandler(long readerIdleTime, long writerIdleTime, long allIdleTime, TimeUnit unit) {
        super(readerIdleTime, writerIdleTime, allIdleTime, unit);
    }

    @Override
    protected void channelIdle(ChannelHandlerContext ctx, IdleStateEvent evt) throws Exception {
//        super.channelIdle(ctx, evt);

        if (evt.state() == IdleState.READER_IDLE) {
            log.info("1s内没有读请求");
        } else if (evt.state() == IdleState.WRITER_IDLE) {
            log.info("1s内没有写请求");
        } else if (evt.state() == IdleState.ALL_IDLE) {
            log.info("1s内没有读写请求");
        } else {
        }
        log.info("客户端超时未响应，断开链接");
        ctx.channel().close();
    }
}
