package xyz.mxdshop.netty.server.handler;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import lombok.extern.slf4j.Slf4j;
import xyz.mxdshop.netty.domain.LoginRequestMessage;
import xyz.mxdshop.netty.domain.LoginResponseMessage;
import xyz.mxdshop.netty.domain.OfflineMessage;
import xyz.mxdshop.netty.domain.OnlineMessage;
import xyz.mxdshop.netty.server.domain.NettyServerSession;
import xyz.mxdshop.netty.server.util.SessionUtil;

@Slf4j
@ChannelHandler.Sharable
public class NettyServerLoginHandler extends SimpleChannelInboundHandler<LoginRequestMessage> {

    private NettyServerLoginHandler() {
    }

    public static NettyServerLoginHandler getInstance() {
        return InstanceHolder.INSTANCE;
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, LoginRequestMessage message) throws Exception {
        log.info("用户：{}, ID：{} 登录服务器", message.getUsername(), message.getUserId());

        // 都登录成功
        LoginResponseMessage loginResponseMessage = new LoginResponseMessage();
        loginResponseMessage.setSuccess(true);
        loginResponseMessage.setUserId(message.getUserId());
        loginResponseMessage.setUsername(message.getUsername());
        ctx.channel().writeAndFlush(loginResponseMessage);

        // 保存session以及session对应的链接
        NettyServerSession session = new NettyServerSession();
        session.setUserId(message.getUserId());
        session.setUsername(message.getUsername());

        SessionUtil.addSession(session.getUserId(), session, ctx.channel());
        // 保存已登录的channel
        ChannelGroup channelGroup = new DefaultChannelGroup(ctx.executor());
        // 通知所有用户有用户上线
        channelGroup.addAll(SessionUtil.getAllChannels());
        channelGroup.writeAndFlush(new OnlineMessage(message.getUserId(), message.getUsername()));
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        sendOffline(ctx);
        SessionUtil.removeChannel(ctx.channel());
        super.channelInactive(ctx);
    }

    private static class InstanceHolder {
        private static final NettyServerLoginHandler INSTANCE = new NettyServerLoginHandler();
    }

    private void sendOffline(ChannelHandlerContext ctx) {
        ChannelGroup channelGroup = new DefaultChannelGroup(ctx.executor());
        // 通知所有用户有用户上线
        channelGroup.addAll(SessionUtil.getAllChannels());
        channelGroup.writeAndFlush(new OfflineMessage(SessionUtil.getUserId(ctx.channel()) == null
                ? 0L : SessionUtil.getUserId(ctx.channel())));
    }
}
