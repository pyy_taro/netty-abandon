package xyz.mxdshop.netty.server.handler;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import lombok.extern.slf4j.Slf4j;
import xyz.mxdshop.netty.domain.HeartBeatRequestMessage;
import xyz.mxdshop.netty.domain.HeartBeatResponseMessage;
import xyz.mxdshop.netty.domain.OnlineListRequestMessage;
import xyz.mxdshop.netty.domain.OnlineListResponseMessage;
import xyz.mxdshop.netty.server.domain.NettyServerSession;
import xyz.mxdshop.netty.server.util.SessionUtil;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@ChannelHandler.Sharable
@Slf4j
public class NettyServerOnlineListHandler extends SimpleChannelInboundHandler<OnlineListRequestMessage> {

    private NettyServerOnlineListHandler() {}

    public static NettyServerOnlineListHandler getInstance() {
        return InstanceHolder.INSTANCE;
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, OnlineListRequestMessage msg) throws Exception {
        log.info("收到客户端请求好友列表消息");
        Collection<NettyServerSession> sessionList = SessionUtil.getAllSessions();
        Map<Long, String> userMap = new HashMap<>(16);
        sessionList.forEach(it -> userMap.putIfAbsent(it.getUserId(), it.getUsername()));
        ctx.channel().writeAndFlush(new OnlineListResponseMessage(userMap));
    }

    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
        log.info("超时了");
    }

    private static class InstanceHolder {
        private static final NettyServerOnlineListHandler INSTANCE = new NettyServerOnlineListHandler();
    }
}
