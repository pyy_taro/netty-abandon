package xyz.mxdshop.netty.server.handler;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import lombok.extern.slf4j.Slf4j;
import xyz.mxdshop.netty.domain.HeartBeatRequestMessage;
import xyz.mxdshop.netty.domain.HeartBeatResponseMessage;

@Slf4j
public class NettyServerHeartBeatHandler extends SimpleChannelInboundHandler<HeartBeatRequestMessage> {

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, HeartBeatRequestMessage msg) throws Exception {
        log.info("收到客户端[{}]心跳，返回回应消息", ctx.channel().remoteAddress());
        ctx.channel().writeAndFlush(new HeartBeatResponseMessage());
    }

    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
        log.info("超时了");
    }
}
