package xyz.mxdshop.netty.server.handler;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import lombok.extern.slf4j.Slf4j;
import xyz.mxdshop.netty.domain.ShakeRequestMessage;
import xyz.mxdshop.netty.domain.ShakeResponseMessage;
import xyz.mxdshop.netty.domain.TextRequestMessage;
import xyz.mxdshop.netty.domain.TextResponseMessage;
import xyz.mxdshop.netty.server.domain.NettyServerSession;
import xyz.mxdshop.netty.server.util.SessionUtil;

@ChannelHandler.Sharable
@Slf4j
public class NettyServerTextMessageHandler extends SimpleChannelInboundHandler<TextRequestMessage> {

    private NettyServerTextMessageHandler() {}

    public static NettyServerTextMessageHandler getInstance() {
        return InstanceHolder.INSTANCE;
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, TextRequestMessage msg) throws Exception {
        NettyServerSession session = SessionUtil.getSession(msg.getToUser());
        if (session != null) {
            TextResponseMessage response = new TextResponseMessage();
            response.setFromUser(msg.getFromUser());
            response.setContent(msg.getContent());
            Channel channel = SessionUtil.getChannel(session);
            channel.writeAndFlush(response);
        }

    }

    private static class InstanceHolder {
        private static final NettyServerTextMessageHandler INSTANCE = new NettyServerTextMessageHandler();
    }
}
