package xyz.mxdshop.netty.server.handler;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import lombok.extern.slf4j.Slf4j;
import xyz.mxdshop.netty.domain.HeartBeatRequestMessage;
import xyz.mxdshop.netty.domain.HeartBeatResponseMessage;
import xyz.mxdshop.netty.domain.ShakeRequestMessage;
import xyz.mxdshop.netty.domain.ShakeResponseMessage;
import xyz.mxdshop.netty.server.domain.NettyServerSession;
import xyz.mxdshop.netty.server.util.SessionUtil;

@ChannelHandler.Sharable
@Slf4j
public class NettyServerShakeMessageHandler extends SimpleChannelInboundHandler<ShakeRequestMessage> {

    private NettyServerShakeMessageHandler() {}

    public static NettyServerShakeMessageHandler getInstance() {
        return InstanceHolder.INSTANCE;
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, ShakeRequestMessage msg) throws Exception {
        NettyServerSession session = SessionUtil.getSession(msg.getToUser());
        if (session != null) {
            ShakeResponseMessage response = new ShakeResponseMessage();
            response.setFromUser(msg.getFromUser());
            Channel channel = SessionUtil.getChannel(session);
            channel.writeAndFlush(response);
        }

    }

    private static class InstanceHolder {
        private static final NettyServerShakeMessageHandler INSTANCE = new NettyServerShakeMessageHandler();
    }
}
