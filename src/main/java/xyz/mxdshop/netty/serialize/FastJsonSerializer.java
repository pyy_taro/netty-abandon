package xyz.mxdshop.netty.serialize;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

/**
 * fast json 序列化
 * @author apple
 */
public class FastJsonSerializer implements Serializer {


    @Override
    public Byte currentSerializeType() {
        return SerializeTypeConstant.JSON;
    }

    @Override
    public byte[] serialize(Object javaObject) {
        return JSON.toJSONBytes(javaObject);
    }

    @Override
    public <T> T deserialize(byte[] bytes, Class<T> clazz) {
        return JSONObject.parseObject(bytes, clazz);
    }
}
