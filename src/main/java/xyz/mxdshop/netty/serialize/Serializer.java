package xyz.mxdshop.netty.serialize;

/**
 * @author apple
 */
public interface Serializer {

    Serializer DEFAULT = new FastJsonSerializer();

    /**
     * 当前的序列化类型
     * @return
     *      return
     */
    Byte currentSerializeType();

    /**
     * java 对象转换成二进制
     * @param javaObject 待转换的Java对象
     * @return byte[]
     */
    byte[] serialize(Object javaObject);

    /**
     * 二进制转换成 java 对象
    * @param bytes 待解码的字节数组
     * @param clazz 反序列化的Java类型
     * @return T
     */
    <T> T deserialize(byte[] bytes, Class<T> clazz);
}
