package xyz.mxdshop.netty.serialize;

/**
 * 序列化类型常量
 * @author apple
 */
public interface SerializeTypeConstant {

    /**
     * json类型
     */
    Byte JSON = 1;
}
