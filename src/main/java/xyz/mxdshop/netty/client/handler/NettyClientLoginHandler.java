package xyz.mxdshop.netty.client.handler;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import lombok.extern.slf4j.Slf4j;
import xyz.mxdshop.netty.client.manager.LoginManager;
import xyz.mxdshop.netty.client.manager.SessionManager;
import xyz.mxdshop.netty.domain.LoginResponseMessage;
import xyz.mxdshop.netty.domain.OnlineListRequestMessage;

@ChannelHandler.Sharable
@Slf4j
public class NettyClientLoginHandler extends SimpleChannelInboundHandler<LoginResponseMessage> {

    private NettyClientLoginHandler() {}

    public static NettyClientLoginHandler getInstance() {
        return InstanceHolder.INSTANCE;
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, LoginResponseMessage msg) throws Exception {
        if (msg.isSuccess()) {
            log.info("登录成功");
            LoginManager.getInstance().handlerLoginSuccess();
            SessionManager.INSTANCE.loginSession(msg.getUserId());
            SessionManager.INSTANCE.sendMessage(new OnlineListRequestMessage(), null);
        } else {
            log.warn("登录失败");
        }
    }

    private static class InstanceHolder {
        private static final NettyClientLoginHandler INSTANCE = new NettyClientLoginHandler();
    }
}
