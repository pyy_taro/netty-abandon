package xyz.mxdshop.netty.client.handler;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import lombok.extern.slf4j.Slf4j;
import xyz.mxdshop.netty.client.manager.SessionManager;
import xyz.mxdshop.netty.client.ui.component.ChatManager;
import xyz.mxdshop.netty.client.vo.UserVo;
import xyz.mxdshop.netty.domain.ShakeResponseMessage;
import xyz.mxdshop.netty.domain.TextResponseMessage;

@ChannelHandler.Sharable
@Slf4j
public class NettyClientTextMessageHandler extends SimpleChannelInboundHandler<TextResponseMessage> {

    private NettyClientTextMessageHandler() {}

    public static NettyClientTextMessageHandler getInstance() {
        return InstanceHolder.INSTANCE;
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, TextResponseMessage msg) throws Exception {
        UserVo friendVo = SessionManager.INSTANCE.getFriend(msg.getFromUser());
        if (friendVo != null) {
            ChatManager.getInstance().receiveFriendPrivateMessage(msg.getFromUser(), msg.getContent());
        }
    }

    private static class InstanceHolder {
        private static final NettyClientTextMessageHandler INSTANCE = new NettyClientTextMessageHandler();
    }
}
