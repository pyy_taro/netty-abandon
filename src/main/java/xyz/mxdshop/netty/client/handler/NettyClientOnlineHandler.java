package xyz.mxdshop.netty.client.handler;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import javafx.beans.property.SimpleStringProperty;
import lombok.extern.slf4j.Slf4j;
import xyz.mxdshop.netty.client.manager.SessionManager;
import xyz.mxdshop.netty.client.vo.UserVo;
import xyz.mxdshop.netty.domain.OnlineMessage;

@ChannelHandler.Sharable
@Slf4j
public class NettyClientOnlineHandler extends SimpleChannelInboundHandler<OnlineMessage> {

    private NettyClientOnlineHandler() {}

    public static NettyClientOnlineHandler getInstance() {
        return InstanceHolder.INSTANCE;
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, OnlineMessage msg) throws Exception {
        log.info("用户{}上线了", msg.getUserId());
        // 不能添加自己
        if (msg.getUserId() != SessionManager.INSTANCE.getCurrentUserId()) {
            UserVo userVo = new UserVo();
            userVo.setUserId(msg.getUserId());
            userVo.setUsername(new SimpleStringProperty("用户 " + msg.getUserId()));
            userVo.setSignature(new SimpleStringProperty("人生路 " + msg.getUserId()));
            SessionManager.INSTANCE.updateFriend(userVo, true);
        }
    }

    private static class InstanceHolder {
        private static final NettyClientOnlineHandler INSTANCE = new NettyClientOnlineHandler();
    }
}
