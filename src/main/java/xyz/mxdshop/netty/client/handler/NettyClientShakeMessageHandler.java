package xyz.mxdshop.netty.client.handler;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import javafx.beans.property.SimpleStringProperty;
import lombok.extern.slf4j.Slf4j;
import xyz.mxdshop.netty.client.manager.SessionManager;
import xyz.mxdshop.netty.client.ui.component.ChatManager;
import xyz.mxdshop.netty.client.ui.component.FriendManager;
import xyz.mxdshop.netty.client.vo.UserVo;
import xyz.mxdshop.netty.domain.OnlineListResponseMessage;
import xyz.mxdshop.netty.domain.ShakeResponseMessage;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@ChannelHandler.Sharable
@Slf4j
public class NettyClientShakeMessageHandler extends SimpleChannelInboundHandler<ShakeResponseMessage> {

    private NettyClientShakeMessageHandler() {}

    public static NettyClientShakeMessageHandler getInstance() {
        return InstanceHolder.INSTANCE;
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, ShakeResponseMessage msg) throws Exception {
        UserVo friendVo = SessionManager.INSTANCE.getFriend(msg.getFromUser());
        if (friendVo != null) {
            ChatManager.getInstance().openChat2PointPanelAsync(friendVo);
        }
    }

    private static class InstanceHolder {
        private static final NettyClientShakeMessageHandler INSTANCE = new NettyClientShakeMessageHandler();
    }
}
