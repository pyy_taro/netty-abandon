package xyz.mxdshop.netty.client.handler;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import javafx.beans.property.SimpleStringProperty;
import lombok.extern.slf4j.Slf4j;
import xyz.mxdshop.netty.client.manager.SessionManager;
import xyz.mxdshop.netty.client.vo.UserVo;
import xyz.mxdshop.netty.domain.OnlineListResponseMessage;
import xyz.mxdshop.netty.domain.OnlineMessage;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@ChannelHandler.Sharable
@Slf4j
public class NettyClientOnlineListHandler extends SimpleChannelInboundHandler<OnlineListResponseMessage> {

    private NettyClientOnlineListHandler() {}

    public static NettyClientOnlineListHandler getInstance() {
        return InstanceHolder.INSTANCE;
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, OnlineListResponseMessage msg) throws Exception {
        // 不能添加自己

        List<UserVo> userList = new ArrayList<>();

        UserVo userVo;
        for (Map.Entry<Long, String> entry : msg.getUserMap().entrySet()) {
            if (entry.getKey() == SessionManager.INSTANCE.getCurrentUserId()) {
                continue;
            }
            userVo = new UserVo();
            userVo.setUserId(entry.getKey());
            userVo.setUsername(new SimpleStringProperty(entry.getValue()));
            userVo.setSignature(new SimpleStringProperty("来自用户" + entry.getKey() + "的签名"));
            userList.add(userVo);
        }
        SessionManager.INSTANCE.updateFriends(userList);
    }

    private static class InstanceHolder {
        private static final NettyClientOnlineListHandler INSTANCE = new NettyClientOnlineListHandler();
    }
}
