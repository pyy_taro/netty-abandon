package xyz.mxdshop.netty.client.handler;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import lombok.extern.slf4j.Slf4j;
import xyz.mxdshop.netty.client.manager.SessionManager;
import xyz.mxdshop.netty.domain.HeartBeatRequestMessage;
import xyz.mxdshop.netty.domain.HeartBeatResponseMessage;
import xyz.mxdshop.netty.domain.TextRequestMessage;

import java.util.concurrent.TimeUnit;

@ChannelHandler.Sharable
@Slf4j
public class NettyClientHeartBeatHandler extends SimpleChannelInboundHandler<HeartBeatResponseMessage> {

    private NettyClientHeartBeatHandler() {

    }

    public static NettyClientHeartBeatHandler getInstance() {
        return NettyClientHeartBeatHandlerHolder.INSTANCE;
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, HeartBeatResponseMessage msg) throws Exception {
        log.info("收到服务端心跳回应, [{}]", msg);
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        SessionManager.INSTANCE.registerSession(ctx.channel());
//        ctx.executor().scheduleWithFixedDelay(() -> {
//            if (ctx.channel().isActive()) {
//                log.info("发送心跳");
//                ctx.channel().writeAndFlush(new HeartBeatRequestMessage());
//            } else {
//                log.info("客户端挂了");
//            }
//        }, 5, 5, TimeUnit.SECONDS);
        TextRequestMessage message = new TextRequestMessage();
        message.setContent("test");
        message.setFromUser(1);
        message.setToUser(2);
        SessionManager.INSTANCE.sendMessage(message, null);
        super.channelActive(ctx);
    }

    private static class NettyClientHeartBeatHandlerHolder {
        private static final NettyClientHeartBeatHandler INSTANCE = new NettyClientHeartBeatHandler();
    }
}
