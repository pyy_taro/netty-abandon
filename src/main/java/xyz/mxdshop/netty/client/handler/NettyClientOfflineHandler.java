package xyz.mxdshop.netty.client.handler;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import javafx.beans.property.SimpleStringProperty;
import lombok.extern.slf4j.Slf4j;
import xyz.mxdshop.netty.client.manager.SessionManager;
import xyz.mxdshop.netty.client.vo.UserVo;
import xyz.mxdshop.netty.domain.OfflineMessage;
import xyz.mxdshop.netty.domain.OnlineMessage;

@ChannelHandler.Sharable
@Slf4j
public class NettyClientOfflineHandler extends SimpleChannelInboundHandler<OfflineMessage> {

    private NettyClientOfflineHandler() {}

    public static NettyClientOfflineHandler getInstance() {
        return InstanceHolder.INSTANCE;
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, OfflineMessage msg) throws Exception {
        log.info("用户{}下线了", msg.getUserId());
        UserVo userVo = new UserVo();
        userVo.setUserId(msg.getUserId());
        SessionManager.INSTANCE.updateFriend(userVo, false);
    }

    private static class InstanceHolder {
        private static final NettyClientOfflineHandler INSTANCE = new NettyClientOfflineHandler();
    }
}
