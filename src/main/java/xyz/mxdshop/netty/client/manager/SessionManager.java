package xyz.mxdshop.netty.client.manager;

import io.netty.channel.Channel;
import io.netty.channel.ChannelPromise;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;
import xyz.mxdshop.netty.client.net.NettyClientSession;
import xyz.mxdshop.netty.client.ui.component.FriendManager;
import xyz.mxdshop.netty.client.vo.UserVo;
import xyz.mxdshop.netty.domain.BasePacket;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

/**
 * 提供一些基础服务接口
 *
 * @author kingston
 */
public enum SessionManager {

    INSTANCE;


    /**
     * 通信会话
     */
    private NettyClientSession session;

    public void registerSession(Channel channel) {
        this.session = new NettyClientSession(channel);
    }

    public void loginSession(long userId) {
        if (this.session != null) {
            this.session.setUserId(userId);
        }
    }

    public void sendMessage(BasePacket packet, GenericFutureListener<Future<Void>> listener) {
        this.session.sendMsg(packet, listener);
    }

    /**
     * 是否已连上服务器
     *
     * @return true 上线
     * false 下线
     */
    public boolean isConnectedSever() {
        return this.session != null;
    }

    public long getCurrentUserId() {
        return this.session != null ? this.session.getUserId() : 0;
    }

    public void updateFriend(UserVo userVo, boolean online) {
        if (this.session != null) {
            if (online) {
                this.session.friendOnline(userVo);
            } else {
                this.session.friendOffline(userVo.getUserId());
            }
        }
    }

    public void updateFriends(List<UserVo> userVoList) {
        if (this.session != null) {
            if (!userVoList.isEmpty()) {
                userVoList.forEach(it -> session.friendOnline(it));
            } else {
                FriendManager.getInstance().friendOnline(this.session.getFriendMap().values());
            }
        }
    }

    public UserVo getFriend(long userId) {
        if (this.session != null) {
            return this.session.getFriend(userId);
        }
        return null;
    }
}
