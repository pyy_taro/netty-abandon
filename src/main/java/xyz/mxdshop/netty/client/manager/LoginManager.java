package xyz.mxdshop.netty.client.manager;

import xyz.mxdshop.netty.client.base.UiBaseService;
import xyz.mxdshop.netty.client.ui.ResourcesConfig;
import xyz.mxdshop.netty.client.ui.StageController;
import xyz.mxdshop.netty.client.ui.component.FriendManager;
import xyz.mxdshop.netty.domain.LoginRequestMessage;

public class LoginManager {

    private LoginManager() {}

    public static LoginManager getInstance() {
        return InstanceHolder.INSTANCE;
    }

    private static class InstanceHolder {
        private static final LoginManager INSTANCE = new LoginManager();
    }

    public void beginToLogin(long userId, String pwd) {
        SessionManager.INSTANCE.sendMessage(
                LoginRequestMessage.builder().userId(userId).username("用户" + userId).password(pwd).build(), null
        );
    }

    public void handlerLoginSuccess() {
        UiBaseService.INSTANCE.runTaskInFxThread(this::redirectToMainPanel);
    }

    private void redirectToMainPanel() {
        StageController stageController = UiBaseService.INSTANCE.getStageController();
        stageController.switchStage(ResourcesConfig.id.MainView, ResourcesConfig.id.LoginView);
    }
}
