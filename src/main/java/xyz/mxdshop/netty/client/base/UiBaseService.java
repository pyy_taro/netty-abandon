package xyz.mxdshop.netty.client.base;

import javafx.application.Platform;
import xyz.mxdshop.netty.client.ui.StageController;

public enum UiBaseService {

	INSTANCE;

	private final StageController stageController = new StageController();

	public StageController getStageController() {
		return stageController;
	}

	/**
	 * 将任务转移给fxapplication线程延迟执行
	 * @param task
	 */
	public void runTaskInFxThread(Runnable task){
		Platform.runLater(task);
	}

}
