package xyz.mxdshop.netty.client.base;

public interface ClientConnectConfig {

    /**
     * 服务器IP
     */
    String HOST = "127.0.0.1";

    /**
     * 服务器端口号
     */
    int PORT = 8888;

    /**
     * 最大重试连接次数
     */
    int MAX_CONNECT_TIMES = 20;
}
