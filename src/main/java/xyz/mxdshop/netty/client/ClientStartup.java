package xyz.mxdshop.netty.client;

import javafx.application.Application;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import xyz.mxdshop.netty.client.base.UiBaseService;
import xyz.mxdshop.netty.client.net.NettyClient;
import xyz.mxdshop.netty.client.ui.ResourcesConfig;
import xyz.mxdshop.netty.client.ui.StageController;

import java.io.IOException;

public class ClientStartup extends Application {

    @Override
    public void init() throws Exception {
//		PacketType.initPackets();
    }

    @Override
    public void start(final Stage stage) throws IOException {
        //与服务端建立连接
        connectToServer();
        StageController stageController = UiBaseService.INSTANCE.getStageController();
        stageController.setPrimaryStage("root", stage);

        Stage loginStage = stageController.loadStage(ResourcesConfig.id.LoginView, ResourcesConfig.layout.LoginView,
                StageStyle.UNDECORATED);

//		stageController.loadStage(ResourcesConfig.id.RegisterView, ResourcesConfig.layout.RegisterView, StageStyle.UNDECORATED);
        Stage mainStage = stageController.loadStage(ResourcesConfig.id.MainView, ResourcesConfig.layout.MainView, StageStyle.UNDECORATED);

        //把主界面放在右上方
        Screen screen = Screen.getPrimary();
        double rightTopX = screen.getVisualBounds().getWidth() * 0.75;
        double rightTopY = screen.getVisualBounds().getHeight() * 0.05;
        mainStage.setX(rightTopX);
        mainStage.setY(rightTopY);

		stageController.loadStage(ResourcesConfig.id.ChatToPoint, ResourcesConfig.layout.ChatToPoint, StageStyle.UTILITY);
//
//		Stage searchStage = stageController.loadStage(R.id.SearchView, R.layout.SeachFriendView,
//				StageStyle.UTILITY);

        //显示MainView舞台
        stageController.setStage(ResourcesConfig.id.LoginView);
//		stageController.setStage(R.id.SearchView);
    }

    private void connectToServer() {
        new Thread(() -> new NettyClient().start()).start();
    }

    public static void main(String[] args) {
        launch();
    }
}
