package xyz.mxdshop.netty.client.net;

import io.netty.channel.Channel;
import io.netty.channel.ChannelPromise;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;
import io.netty.util.concurrent.Promise;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import xyz.mxdshop.netty.client.ui.component.FriendManager;
import xyz.mxdshop.netty.client.vo.UserVo;
import xyz.mxdshop.netty.domain.BasePacket;

import java.util.List;
import java.util.TreeMap;

@NoArgsConstructor
@Data
public class NettyClientSession {

    private long userId;

    private Channel channel;

    /**
     * 好友列表
     */
    private TreeMap<Long, UserVo> friendMap = new TreeMap<>();

    public NettyClientSession(Channel channel) {
        this.channel = channel;
    }

    public void sendMsg(BasePacket packet, GenericFutureListener<Future<Void>> listener) {
        if (listener != null) {
            channel.writeAndFlush(packet).addListener(listener);
        } else {
            channel.writeAndFlush(packet);
        }
    }

    public void friendOnline(UserVo userVo) {
        this.friendMap.putIfAbsent(userVo.getUserId(), userVo);
        FriendManager.getInstance().friendOnline(friendMap.values());
    }

    public void friendOffline(Long userId) {
        this.friendMap.remove(userId);
        FriendManager.getInstance().friendOnline(friendMap.values());
    }

    public UserVo getFriend(Long userId) {
        return friendMap.get(userId);
    }
}
