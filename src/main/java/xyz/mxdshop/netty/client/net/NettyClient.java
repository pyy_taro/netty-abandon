package xyz.mxdshop.netty.client.net;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import lombok.extern.slf4j.Slf4j;
import netscape.security.UserTarget;
import xyz.mxdshop.netty.client.base.ClientConnectConfig;
import xyz.mxdshop.netty.client.handler.*;
import xyz.mxdshop.netty.client.manager.SessionManager;
import xyz.mxdshop.netty.domain.TextRequestMessage;
import xyz.mxdshop.netty.transform.PacketDecoder;
import xyz.mxdshop.netty.transform.PacketEncoder;

import java.util.concurrent.TimeUnit;

/**
 * @author pyy
 */
@Slf4j
public class NettyClient {

    private int reconnectTimes = 0;

    Bootstrap bootstrap = new Bootstrap();

    public void connect(String host, int port) {
        NioEventLoopGroup nioEventLoopGroup = new NioEventLoopGroup();
        try {

            bootstrap.group(nioEventLoopGroup)
                    .channel(NioSocketChannel.class)
                    .handler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel ch) {
                            ch.pipeline().addLast(new PacketDecoder());
                            ch.pipeline().addLast(NettyClientHeartBeatHandler.getInstance());
                            ch.pipeline().addLast(NettyClientLoginHandler.getInstance());
                            ch.pipeline().addLast(NettyClientOnlineHandler.getInstance());
                            ch.pipeline().addLast(NettyClientOfflineHandler.getInstance());
                            ch.pipeline().addLast(NettyClientOnlineListHandler.getInstance());
                            ch.pipeline().addLast(NettyClientShakeMessageHandler.getInstance());
                            ch.pipeline().addLast(NettyClientTextMessageHandler.getInstance());
                            ch.pipeline().addLast(new PacketEncoder());
                        }
                    }).connect(host, port).addListener(
                    future -> {
                        if (future.isSuccess()) {
                            log.info("连接服务器成功");
                        } else {
                            log.error("链接服务器失败");
                            if (reconnectTimes < ClientConnectConfig.MAX_CONNECT_TIMES) {
                                reConnect();
                            }
                        }
                    }
            ).channel().closeFuture().sync();
        } catch (Exception e) {
            log.error("链接服务器异常", e);
        } finally {
            nioEventLoopGroup.shutdownGracefully();
        }
    }

    private void reConnect() {
        try {
            log.warn("连接失败，第{}次重试中...", reconnectTimes);
            TimeUnit.SECONDS.sleep(3);
            reconnectTimes++;
            connect(ClientConnectConfig.HOST, ClientConnectConfig.PORT);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void start() {
        connect(ClientConnectConfig.HOST, ClientConnectConfig.PORT);
    }

    public static void main(String[] args) {
        new NettyClient().start();
    }
}
