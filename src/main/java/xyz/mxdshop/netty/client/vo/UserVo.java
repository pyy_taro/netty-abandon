package xyz.mxdshop.netty.client.vo;

import javafx.beans.property.StringProperty;
import lombok.Data;

@Data
public class UserVo {

    private long userId;

    private StringProperty username;

    private StringProperty signature;
}
