package xyz.mxdshop.netty.client.ui;

import javafx.stage.Stage;

public interface ControlledStage {

	Stage getMyStage();

}
