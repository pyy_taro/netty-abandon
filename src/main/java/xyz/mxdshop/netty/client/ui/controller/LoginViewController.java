package xyz.mxdshop.netty.client.ui.controller;

import javafx.beans.binding.Bindings;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import xyz.mxdshop.netty.client.manager.LoginManager;
import xyz.mxdshop.netty.client.manager.SessionManager;
import xyz.mxdshop.netty.client.base.UiBaseService;
import xyz.mxdshop.netty.client.ui.ControlledStage;
import xyz.mxdshop.netty.client.ui.ResourcesConfig;
import xyz.mxdshop.netty.client.ui.StageController;
import xyz.mxdshop.netty.client.ui.container.ResourceContainer;
import xyz.mxdshop.netty.client.util.I18n;
import xyz.mxdshop.netty.client.util.NumberUtil;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class LoginViewController implements ControlledStage, Initializable {

	@FXML
	private Button login;
	@FXML
	private TextField userId;
	@FXML
	private PasswordField password;
	@FXML
	private CheckBox rememberPsw;
	@FXML
	private CheckBox autoLogin;
	@FXML
	private ImageView closeBtn;
	@FXML
	private ImageView minBtn;
	@FXML
	private ProgressBar loginProgress;
	@FXML
	private Pane errorPane;
	@FXML
	private Label errorTips;

	private double oldScreenX = 0.0d;
	private double oldScreenY = 0.0d;
	private double oldStageX = 0.0d;
	private double oldStageY = 0.0d;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		//验证规则：　userId非空且为数字　password非空
		login.disableProperty().bind(
			Bindings.createBooleanBinding(
				() -> userId.getText().length() == 0 ||
					  !NumberUtil.isInteger(userId.getText()) ||
					  password.getText().length() == 0,
				userId.textProperty(),
				password.textProperty()));
	}

	@FXML
	private void login() throws IOException {
		final long useId = Long.parseLong(userId.getText());
		final String psw = password.getText();

		if (!SessionManager.INSTANCE.isConnectedSever()) {
			errorPane.setVisible(true);
			errorTips.setText(I18n.get("login.failToConnect"));
			return;
		}

		loginProgress.setVisible(true);
		login.setVisible(false);

		LoginManager.getInstance().beginToLogin(useId, psw);
	}

	@FXML
	private void close() {
		System.exit(1);
	}

	@FXML
	private void min() {
		Stage stage = getMyStage();
		if (stage != null) {
			stage.setIconified(true);
		}
	}

	@FXML
	private void closeEntered() {
		Image image = ResourceContainer.getClose_1();
		closeBtn.setImage(image);
	}

	@FXML
	private void closeExited() {
		Image image = ResourceContainer.getClose();
		closeBtn.setImage(image);
	}

	@FXML
	private void minEntered() {
		Image image = ResourceContainer.getMin_1();
		minBtn.setImage(image);
	}

	@FXML
	private void minExited() {
		Image image = ResourceContainer.getMin();
		minBtn.setImage(image);
	}

	@FXML
	private void backToLogin() {
		loginProgress.setVisible(false);
		errorPane.setVisible(false);
		login.setVisible(true);
	}

	@FXML
	private void login_en() {
		login.setStyle("-fx-background-radius:4;-fx-background-color: #097299");
	}

	@FXML
	private void login_ex() {
		login.setStyle("-fx-background-radius:4;-fx-background-color: #09A3DC");
	}

	@FXML
	private void gotoRegister() {
		StageController stageController = UiBaseService.INSTANCE.getStageController();
		stageController.switchStage(ResourcesConfig.id.RegisterView, ResourcesConfig.id.LoginView);
	}

	@FXML
	private void mousePressed(MouseEvent e) {
		oldStageX = this.getMyStage().getX();
		oldStageY = this.getMyStage().getY();
		oldScreenX = e.getScreenX();
		oldScreenY = e.getScreenY();
	}

	@FXML
	private void mouseDrag(MouseEvent e) {

		this.getMyStage().setX(oldStageX + e.getScreenX() - oldScreenX);
		this.getMyStage().setY(oldStageY + e.getScreenY() - oldScreenY);
	}

	@Override
	public Stage getMyStage() {
		StageController stageController = UiBaseService.INSTANCE.getStageController();
		return stageController.getStageBy(ResourcesConfig.id.LoginView);
	}

}
