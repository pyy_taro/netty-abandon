package xyz.mxdshop.netty.client.ui.component;

import io.netty.channel.ChannelPromise;
import io.netty.channel.DefaultChannelPromise;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import lombok.extern.slf4j.Slf4j;
import xyz.mxdshop.netty.client.base.UiBaseService;
import xyz.mxdshop.netty.client.manager.SessionManager;
import xyz.mxdshop.netty.client.ui.ResourcesConfig;
import xyz.mxdshop.netty.client.ui.StageController;
import xyz.mxdshop.netty.client.vo.UserVo;
import xyz.mxdshop.netty.domain.ShakeRequestMessage;
import xyz.mxdshop.netty.domain.TextRequestMessage;

import java.text.SimpleDateFormat;
import java.util.Date;


@Slf4j
public class ChatManager {


    private ChatManager() {
    }

    public void sendShakeMessage(long toUser) {
        ShakeRequestMessage request = new ShakeRequestMessage();
        request.setFromUser(SessionManager.INSTANCE.getCurrentUserId());
        request.setToUser(toUser);
        SessionManager.INSTANCE.sendMessage(request, null);
    }

    public void openChat2PointPanelAsync(UserVo targetFriend) {
        StageController stageController = UiBaseService.INSTANCE.getStageController();
        UiBaseService.INSTANCE.runTaskInFxThread(() -> FriendManager.getInstance().openChat2PointPanel(targetFriend));

        try {
            UiBaseService.INSTANCE.runTaskInFxThread(() -> stageController.setLocation(ResourcesConfig.id.ChatToPoint, 50, 0));
            Thread.sleep(100);
            UiBaseService.INSTANCE.runTaskInFxThread(() -> stageController.setLocation(ResourcesConfig.id.ChatToPoint, -50, 0));
            Thread.sleep(90);
            UiBaseService.INSTANCE.runTaskInFxThread(() -> stageController.setLocation(ResourcesConfig.id.ChatToPoint, 50, 0));
            Thread.sleep(80);
            UiBaseService.INSTANCE.runTaskInFxThread(() -> stageController.setLocation(ResourcesConfig.id.ChatToPoint, -50, 0));
            Thread.sleep(70);
            UiBaseService.INSTANCE.runTaskInFxThread(() -> stageController.setLocation(ResourcesConfig.id.ChatToPoint, 50, 0));
            Thread.sleep(60);
            UiBaseService.INSTANCE.runTaskInFxThread(() -> stageController.setLocation(ResourcesConfig.id.ChatToPoint, -50, 0));
            Thread.sleep(50);
            UiBaseService.INSTANCE.runTaskInFxThread(() -> stageController.setLocation(ResourcesConfig.id.ChatToPoint, 50, 0));
            Thread.sleep(40);
            UiBaseService.INSTANCE.runTaskInFxThread(() -> stageController.setLocation(ResourcesConfig.id.ChatToPoint, -50, 0));
            Thread.sleep(30);
            UiBaseService.INSTANCE.runTaskInFxThread(() -> stageController.setLocation(ResourcesConfig.id.ChatToPoint, 50, 0));
            Thread.sleep(20);
            UiBaseService.INSTANCE.runTaskInFxThread(() -> stageController.setLocation(ResourcesConfig.id.ChatToPoint, -50, 0));
        } catch (Exception e) {
            log.error("抖动异常", e);
        }
    }

    public void sendTextMessage(long toUser, String content, GenericFutureListener<Future<Void>> listener) {

        TextRequestMessage request = new TextRequestMessage();
        request.setFromUser(SessionManager.INSTANCE.getCurrentUserId());
        request.setToUser(toUser);
        request.setContent(content);
        SessionManager.INSTANCE.sendMessage(request, listener);
    }

    public void receiveFriendPrivateMessage(long sourceId, String content) {
        StageController stageController = UiBaseService.INSTANCE.getStageController();
        Stage stage = stageController.getStageBy(ResourcesConfig.id.ChatToPoint);
        VBox msgContainer = (VBox)stage.getScene().getRoot().lookup("#msgContainer");

        UiBaseService.INSTANCE.runTaskInFxThread(()-> {
            Pane pane = null;
            if (sourceId == SessionManager.INSTANCE.getCurrentUserId()) {
                pane = stageController.load(ResourcesConfig.layout.PrivateChatItemRight, Pane.class);
            }else {
                pane = stageController.load(ResourcesConfig.layout.PrivateChatItemLeft, Pane.class);
            }

            decorateChatRecord(content, pane);
            msgContainer.getChildren().add(pane);
        });

    }

    public static ChatManager getInstance() {
        return InstanceHolder.INSTANCE;
    }


    private void decorateChatRecord(String message, Pane chatRecord) {
        Hyperlink nickname = (Hyperlink) chatRecord.lookup("#nameUi");
        nickname.setText(message);
        nickname.setVisible(false);
        Label createTime = (Label) chatRecord.lookup("#timeUi");
        createTime.setText(new SimpleDateFormat("yyyy年MM月dd日  HH:mm:ss").format(new Date()));
        Label body = (Label) chatRecord.lookup("#contentUi");
        body.setText(message);
    }

    private static class InstanceHolder {

        private static final ChatManager INSTANCE = new ChatManager();
    }
}
