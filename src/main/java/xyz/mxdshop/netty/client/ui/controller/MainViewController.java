package xyz.mxdshop.netty.client.ui.controller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Accordion;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TitledPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import xyz.mxdshop.netty.client.base.UiBaseService;
import xyz.mxdshop.netty.client.manager.SessionManager;
import xyz.mxdshop.netty.client.ui.ControlledStage;
import xyz.mxdshop.netty.client.ui.ResourcesConfig;
import xyz.mxdshop.netty.client.ui.StageController;
import xyz.mxdshop.netty.client.ui.container.ResourceContainer;
import xyz.mxdshop.netty.client.vo.UserVo;

import java.net.URL;
import java.util.ResourceBundle;

public class MainViewController implements ControlledStage, Initializable {

    @FXML
    private ImageView close;
    @FXML
    private ImageView min;
    @FXML
    private ImageView shineImage;
    @FXML
    private Accordion friends;
    @FXML
    private ScrollPane friendSp;
    @FXML
    private Label username;
    @FXML
    private Label signature;
    @FXML
    private Label topic;

    private double oldScreenX = 0.0d;
    private double oldScreenY = 0.0d;
    private double oldStageX = 0.0d;
    private double oldStageY = 0.0d;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        topic.setText("Netty");
		username.textProperty().bind(new SimpleStringProperty("用户名"));
		signature.textProperty().bind(new SimpleStringProperty("走的人多了，也就有了路"));
    }

    @FXML
    private void close() {
        System.exit(1);
    }

    @FXML
    private void closeEntered() {
        Image image = ResourceContainer.getClose_1();
        close.setImage(image);
    }

    @FXML
    private void closeExited() {
        Image image = ResourceContainer.getClose();
        close.setImage(image);
    }

    @FXML
    private void minEntered() {
        Image image = ResourceContainer.getMin_1();
        min.setImage(image);
    }

    @FXML
    private void minExited() {
        Image image = ResourceContainer.getMin();
        min.setImage(image);
    }

    @FXML
    private void bind() {
        friendSp.setFitToWidth(false);
        friends.expandedPaneProperty().addListener((arg0, arg1, arg2) -> {
            if (arg2 != null) {
                System.out.println("-------11111111--------");
            }
            if (arg1 != null) {
                System.out.println("-------2222222222---------");
            }
        });
    }

    @FXML
    private void min() {
        getMyStage().setIconified(true);
    }

    @FXML
    private void username_entered() {
        username.setStyle("-fx-background-radius:4;-fx-background-color: #136f9b");
    }

    @FXML
    private void username_exited() {
        username.setStyle("");
    }

    @FXML
    private void autograph_entered() {
        signature.setStyle("-fx-background-radius:4;-fx-background-color: #136f9b");
    }

    @FXML
    private void autograph_exited() {
        signature.setStyle("");
    }

    @FXML
    private void headEx() {
        shineImage.setVisible(false);
    }

    @FXML
    private void headEn() {
        shineImage.setVisible(true);
    }

    @FXML
    private void queryEvent() {
//		SearchManager.getInstance().refreshRecommendFriends(new ArrayList<>());
    }

    @FXML
    private void mousePressed(MouseEvent e) {
        oldStageX = this.getMyStage().getX();
        oldStageY = this.getMyStage().getY();
        oldScreenX = e.getScreenX();
        oldScreenY = e.getScreenY();
    }

    @FXML
    private void mouseDrag(MouseEvent e) {

        this.getMyStage().setX(oldStageX + e.getScreenX() - oldScreenX);
        this.getMyStage().setY(oldStageY + e.getScreenY() - oldScreenY);
    }

    @Override
    public Stage getMyStage() {
        StageController stageController = UiBaseService.INSTANCE.getStageController();
        return stageController.getStageBy(ResourcesConfig.id.MainView);
    }
}
