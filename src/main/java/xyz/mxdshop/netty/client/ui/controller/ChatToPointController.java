package xyz.mxdshop.netty.client.ui.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import lombok.extern.slf4j.Slf4j;
import xyz.mxdshop.netty.client.base.UiBaseService;
import xyz.mxdshop.netty.client.manager.SessionManager;
import xyz.mxdshop.netty.client.ui.ControlledStage;
import xyz.mxdshop.netty.client.ui.ResourcesConfig;
import xyz.mxdshop.netty.client.ui.StageController;
import xyz.mxdshop.netty.client.ui.component.ChatManager;
import xyz.mxdshop.netty.client.ui.container.ResourceContainer;

import java.io.IOException;

@Slf4j
public class ChatToPointController implements ControlledStage {

	@FXML
	private Label userIdUi;

	@FXML
	private TextArea msgInput;

	@FXML
	private ScrollPane outputMsgUi;

	@FXML
	private ImageView toolbarEmoticon;

	@FXML
	private ImageView toolbarShake;

	@FXML
	private void sendMessage() {
		final long userId = Long.parseLong(userIdUi.getText());
		String message = msgInput.getText();
		ChatManager.getInstance().sendTextMessage(userId, message, future -> {
			if (future.isSuccess()) {
				msgInput.setText("");
				ChatManager.getInstance().receiveFriendPrivateMessage(SessionManager.INSTANCE.getCurrentUserId(), message);
			}
		});
	}


	@Override
	public Stage getMyStage() {
		StageController stageController = UiBaseService.INSTANCE.getStageController();
		return stageController.getStageBy(ResourcesConfig.id.ChatToPoint);
	}

	@FXML
	private void close() {
		UiBaseService.INSTANCE.getStageController().closeStage(ResourcesConfig.id.ChatToPoint);
	}

	@FXML
	private void toolbarEmoticonEnter() {
		Image image = ResourceContainer.getToolbarEmoticonEnter();
		toolbarEmoticon.setImage(image);
	}

	@FXML
	private void toolbarEmoticonNormal() {
		Image image = ResourceContainer.getToolbarEmoticonNormal();
		toolbarEmoticon.setImage(image);
	}

	@FXML
	private void toolbarEmoticonDown() {
		Image image = ResourceContainer.getToolbarEmoticonDown();
		toolbarEmoticon.setImage(image);
	}

	@FXML
	private void toolbarShakeEnter() {
		Image image = ResourceContainer.TOOLBAR_SHAKE_ENTER;
		toolbarShake.setImage(image);
	}

	@FXML
	private void toolbarShakeNormal() {
		Image image = ResourceContainer.TOOLBAR_SHAKE_NORMAL;
		toolbarShake.setImage(image);
	}

	@FXML
	private void toolbarShakeDown() {
		Image image = ResourceContainer.TOOLBAR_SHAKE_DOWN;
		toolbarShake.setImage(image);
	}

	@FXML
	private void toolbarShakeClick(MouseEvent e) {
		long userId = Long.parseLong(userIdUi.getText());
		ChatManager.getInstance().sendShakeMessage(userId);
	}
}


