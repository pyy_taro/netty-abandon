package xyz.mxdshop.netty.client.ui.component;

import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import lombok.extern.slf4j.Slf4j;
import xyz.mxdshop.netty.client.base.UiBaseService;
import xyz.mxdshop.netty.client.manager.SessionManager;
import xyz.mxdshop.netty.client.ui.ResourcesConfig;
import xyz.mxdshop.netty.client.ui.StageController;
import xyz.mxdshop.netty.client.vo.UserVo;

import java.util.Collection;
import java.util.concurrent.TimeUnit;


@Slf4j
public class FriendManager {

    private int clickCount = 0;

    private long lastClientTime;

    private static final long INTERVAL = 200L;

    private FriendManager() {
    }

    public void friendOnline(Collection<UserVo> userVoList) {
        UiBaseService.INSTANCE.runTaskInFxThread(() -> doFriendOnline(userVoList));
    }

    public static FriendManager getInstance() {
        return InstanceHolder.INSTANCE;
    }

    private void doFriendOnline(Collection<UserVo> userVoList) {
        StageController stageController = UiBaseService.INSTANCE.getStageController();
        Stage stage = stageController.getStageBy(ResourcesConfig.id.MainView);
        ScrollPane scrollPane = (ScrollPane) stage.getScene().getRoot().lookup("#friendSp");
        Accordion friendGroup = (Accordion) scrollPane.getContent();
        friendGroup.getPanes().clear();

        decorateFriendGroup(friendGroup, userVoList);
    }

    private void decorateFriendGroup(Accordion container, Collection<UserVo> userVoList) {
        ListView<Node> listView = new ListView<>();
        StageController stageController = UiBaseService.INSTANCE.getStageController();
        if (userVoList != null) {
            for (UserVo userVo : userVoList) {
                Pane pane = stageController.load(ResourcesConfig.layout.FriendItem, Pane.class);
                decorateFriendItem(pane, userVo);
                listView.getItems().add(pane);
            }
        }

        bindDoubleClickEvent(listView);
        String groupInfo = "我的好友";
        TitledPane tp = new TitledPane(groupInfo, listView);
        container.getPanes().add(tp);
    }

    private void decorateFriendItem(Pane itemUi, UserVo userVo) {
        Label autographLabel = (Label) itemUi.lookup("#signature");
        autographLabel.setText(userVo.getSignature().get());
        Hyperlink usernameUi = (Hyperlink) itemUi.lookup("#userName");
        usernameUi.setText(userVo.getUsername().get());

        //隐藏域，聊天界面用
        Label userIdUi = (Label) itemUi.lookup("#friendId");
        userIdUi.setText(userVo.getUserId() + "");

//        ImageView headImage = (ImageView) itemUi.lookup("#headIcon");

//        headImage.setImage(ImageUtil.convertToGray(headImage.getImage()));

    }


    private void bindDoubleClickEvent(ListView<Node> listView) {
        listView.setOnMouseClicked(event -> {
            ++clickCount;
            lastClientTime = System.currentTimeMillis();
            if (clickCount % 2 == 0) {
                long diff = System.currentTimeMillis() - lastClientTime;
                if (diff < INTERVAL) {
                    ListView<Node> view = (ListView<Node>) event.getSource();
                    Node selectedItem = view.getSelectionModel().getSelectedItem();
                    if (selectedItem == null)
                        return;
                    Pane pane = (Pane) selectedItem;
                    Label userIdUi = (Label) pane.lookup("#friendId");

                    long friendId = Long.parseLong(userIdUi.getText());
                    UserVo targetFriend = SessionManager.INSTANCE.getFriend(friendId);

                    long selfId = SessionManager.INSTANCE.getCurrentUserId();
                    if (friendId == selfId) {
                        //不能跟自己聊天
                        return;
                    }
                    if (targetFriend != null) {
                        openChat2PointPanel(targetFriend);
                    }
                }
            }
        });
    }

    public void openChat2PointPanel(UserVo targetFriend) {
        StageController stageController = UiBaseService.INSTANCE.getStageController();
        Stage chatStage = stageController.setStage(ResourcesConfig.id.ChatToPoint);

        Label userIdUi = (Label) chatStage.getScene().getRoot().lookup("#userIdUi");
        userIdUi.setText(String.valueOf(targetFriend.getUserId()));
        Hyperlink userNameUi = (Hyperlink) chatStage.getScene().getRoot().lookup("#userName");
        Label signatureUi = (Label) chatStage.getScene().getRoot().lookup("#signature");
        userNameUi.setText(targetFriend.getUsername().get());
        signatureUi.setText(targetFriend.getSignature().get());
    }

    private static class InstanceHolder {

        private static final FriendManager INSTANCE = new FriendManager();
    }
}
