package xyz.mxdshop.netty.client.ui.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import xyz.mxdshop.netty.client.base.UiBaseService;
import xyz.mxdshop.netty.client.ui.ControlledStage;
import xyz.mxdshop.netty.client.ui.ResourcesConfig;
import xyz.mxdshop.netty.client.ui.StageController;

import java.net.URL;
import java.util.ResourceBundle;

public class SearchViewController implements ControlledStage, Initializable {

	@FXML
	private GridPane friendsContainer;

	@FXML
	private TextField friendKey;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub

	}

	@FXML
	public void onSearchBtnClicked() {
//		String key = friendKey.getText();
//		ReqSearchFriends req = new ReqSearchFriends();
//		req.setKey(key);
//		SessionManager.INSTANCE.sendMessage(req);
	}


	@FXML
	private void close() {
		StageController stageController = UiBaseService.INSTANCE.getStageController();
		stageController.closeStage(ResourcesConfig.id.SearchView);
	}

	@Override
	public Stage getMyStage() {
		StageController stageController = UiBaseService.INSTANCE.getStageController();
		return stageController.getStageBy(ResourcesConfig.id.SearchView);
	}

}
