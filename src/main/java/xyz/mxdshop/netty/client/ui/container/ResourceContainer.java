package xyz.mxdshop.netty.client.ui.container;

import javafx.scene.image.Image;

import java.util.Objects;

public class ResourceContainer {
	
	private static Image close = getImage("login/img/close.png");
	private static Image close_1 = getImage("login/img/close_1.png");
	private static Image min = getImage("login/img/min.png");
	private static Image min_1 = getImage("login/img/min_1.png");

	private static Image toolbarEmoticonNormal = getImage("main/img/chat/toolbar_emoticon_normal.png");
	private static Image toolbarEmoticonDown = getImage("main/img/chat/toolbar_emoticon_down.png");
	private static Image toolbarEmoticonEnter = getImage("main/img/chat/toolbar_emoticon_hover.png");
	public static final Image TOOLBAR_SHAKE_NORMAL = getImage("main/img/chat/toolbar_shake_normal.png");
	public static final Image TOOLBAR_SHAKE_DOWN = getImage("main/img/chat/toolbar_shake_down.png");
	public static final Image TOOLBAR_SHAKE_ENTER = getImage("main/img/chat/toolbar_shake_hover.png");
	private static Image creator = getImage("main/img/creator.png");
	private static Image admin = getImage("main/img/admin.png");
	private static Image headImg = getImage("main/img/headImg.png");

	private ResourceContainer() {}

	public static Image getClose() {
		return close;
	}

	public static Image getClose_1() {
		return close_1;
	}

	public static Image getMin() {
		return min;
	}

	public static Image getMin_1() {
		return min_1;
	}

	public static Image getToolbarEmoticonNormal() {
		return toolbarEmoticonNormal;
	}

	public static Image getToolbarEmoticonEnter() {
		return toolbarEmoticonEnter;
	}

	public static Image getToolbarEmoticonDown() {
		return toolbarEmoticonDown;
	}

	public static Image getCreator() {
		return creator;
	}

	public static Image getAdmin() {
		return admin;
	}

	public static Image getHeadImg() {
		return headImg;
	}

	private static Image getImage(String resourcePath) {
		return new Image(Objects.requireNonNull(Thread.currentThread().getContextClassLoader().getResourceAsStream(resourcePath)));

	}
}
