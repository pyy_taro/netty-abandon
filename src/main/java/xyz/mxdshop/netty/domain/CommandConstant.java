package xyz.mxdshop.netty.domain;

/**
 * 指令常量
 * @author apple
 */
public interface CommandConstant {

    Byte LOGIN_REQ = 1;

    Byte LOGIN_RSP = 2;

    Byte ONLINE = 3;

    Byte OFFLINE = 4;

    Byte ONLINE_LIST_REQ = 5;

    Byte ONLINE_LIST_RSP = 6;

    Byte SHAKE_REQ = 7;

    Byte SHAKE_RSP = 8;

    Byte TEXT_REQ = 9;

    Byte TEXT_RSP = 10;

    /**
     * 心跳请求
     */
    Byte HEARTBEAT_REQ = 121;

    /**
     * 心跳返回
     */
    Byte HEARTBEAT_RSP = 122;
}
