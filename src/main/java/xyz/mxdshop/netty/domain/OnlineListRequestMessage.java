package xyz.mxdshop.netty.domain;

public class OnlineListRequestMessage extends BasePacket {

    @Override
    public Byte getCommand() {
        return CommandConstant.ONLINE_LIST_REQ;
    }
}
