package xyz.mxdshop.netty.domain;

import lombok.Data;

@Data
public class TextResponseMessage extends BasePacket {

    private long fromUser;

    private String content;

    @Override
    public Byte getCommand() {
        return CommandConstant.TEXT_RSP;
    }
}
