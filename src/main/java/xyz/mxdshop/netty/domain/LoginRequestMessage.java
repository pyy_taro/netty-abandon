package xyz.mxdshop.netty.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 登录消息
 * @author pyy
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoginRequestMessage extends BasePacket {

    private long userId;

    private String username;

    private String password;

    @Override
    public Byte getCommand() {
        return CommandConstant.LOGIN_REQ;
    }
}
