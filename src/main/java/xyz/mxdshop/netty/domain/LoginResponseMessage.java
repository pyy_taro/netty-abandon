package xyz.mxdshop.netty.domain;

import lombok.Data;

/**
 * @author pyy
 * @date 2020-08-08 16:41
 **/
@Data
public class LoginResponseMessage extends BasePacket {

    private long userId;

    private String username;

    private boolean success;

    private String errMsg;

    @Override
    public Byte getCommand() {
        return CommandConstant.LOGIN_RSP;
    }
}
