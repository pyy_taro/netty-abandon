package xyz.mxdshop.netty.domain;

import lombok.Data;

@Data
public class TextRequestMessage extends BasePacket {

    private long fromUser;

    private long toUser;

    private String content;

    @Override
    public Byte getCommand() {
        return CommandConstant.TEXT_REQ;
    }
}
