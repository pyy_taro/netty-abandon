package xyz.mxdshop.netty.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class OfflineMessage extends BasePacket {

    private long userId;

    @Override
    public Byte getCommand() {
        return CommandConstant.OFFLINE;
    }
}
