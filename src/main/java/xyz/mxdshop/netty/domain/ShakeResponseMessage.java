package xyz.mxdshop.netty.domain;

import lombok.Data;

@Data
public class ShakeResponseMessage extends BasePacket {

    private long fromUser;

    @Override
    public Byte getCommand() {
        return CommandConstant.SHAKE_RSP;
    }
}
