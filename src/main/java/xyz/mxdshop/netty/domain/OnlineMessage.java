package xyz.mxdshop.netty.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author pyy
 * @date 2020-08-08 16:23
 **/
@Data
@AllArgsConstructor
public class OnlineMessage extends BasePacket {

    private long userId;

    private String username;

    @Override
    public Byte getCommand() {
        return CommandConstant.ONLINE;
    }
}
