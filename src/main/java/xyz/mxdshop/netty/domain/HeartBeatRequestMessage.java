package xyz.mxdshop.netty.domain;


import static xyz.mxdshop.netty.domain.CommandConstant.HEARTBEAT_REQ;

public class HeartBeatRequestMessage extends BasePacket {

    @Override
    public Byte getCommand() {
        return HEARTBEAT_REQ;
    }
}
