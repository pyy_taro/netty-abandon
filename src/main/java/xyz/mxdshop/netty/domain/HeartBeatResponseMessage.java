package xyz.mxdshop.netty.domain;


import static xyz.mxdshop.netty.domain.CommandConstant.HEARTBEAT_RSP;

public class HeartBeatResponseMessage extends BasePacket {
    @Override
    public Byte getCommand() {
        return HEARTBEAT_RSP;
    }
}
