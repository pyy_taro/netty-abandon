package xyz.mxdshop.netty.domain;

import lombok.Data;

/**
 * 数据包基础类
 * @author apple
 */
@Data
public abstract class BasePacket {

    /**
     * 获取当前的操作指令
     * @return
     *      return
     */
    public abstract Byte getCommand();
}
