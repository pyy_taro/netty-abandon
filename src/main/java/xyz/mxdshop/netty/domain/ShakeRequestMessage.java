package xyz.mxdshop.netty.domain;

import lombok.Data;

@Data
public class ShakeRequestMessage extends BasePacket {

    private long fromUser;

    private long toUser;

    @Override
    public Byte getCommand() {
        return CommandConstant.SHAKE_REQ;
    }
}
