package xyz.mxdshop.netty.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class OnlineListResponseMessage extends BasePacket {

    private Map<Long, String> userMap;

    @Override
    public Byte getCommand() {
        return CommandConstant.ONLINE_LIST_RSP;
    }
}
